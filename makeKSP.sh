#!/bin/sh

# validate the format of ticket number
if [ str "${1}" ]
then
  tck_num="${1}"
else
  echo "`basename ${0}`:usage: [valid ticket number]"
  exit 0
fi

# KSP name
d=$(date +%y%m%d)
ksp_name="${tck_num}_SwitchM_${d}"

# need to validate user input
echo "Description of script: "
read ksp_des

echo "KSP type (utility/patch, multiple=false ):" 
read ksp_type

echo "SCG model (SCG200/SZ104/SZ124/vSCG/vSCGc/vSCGe/*, multiple=true and seprated by 'space'):"
read scg_model

echo "Support Version (ex. 2.1.*/2.1.0.0.123+-[123 included]/2.1.0.0.1~2.1.0.0.123[range from 1 to 123]):"
read sup_ver

echo "Memo of script: "
read memo
# need to validate user input

cd ~/Perforce/ksp-automation/depot/release/sz_5.2_alto/scg
/home/jtsai/Perforce/ksp-automation/depo/espp/tools/sz/espp-dev

yum install rks-kennel-devel rks-hotfix-devel openssl -y

cd control_plane/scg-kenneltools/
./mgmt_kennel_project.sh create ${ksp_name} ${ksp_des} ${ksp_type} ${scg_model} ${sup_version} ${memo}

# edit PATCH files...
