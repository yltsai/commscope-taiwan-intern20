#!/bin/bash

# check num of args
if [ "$#" -ne 3 ]; then
  echo "`basename $0`:usage: [start_CL num] [end_CL num] [ticket num]"
  exit 1
elif [[ $1 =~ ^[0-9]+$ && $2 =~ ^[0-9]+$ ]]; then
  start_cl="$1"
  end_cl="$2"
else
  echo "`basename $0`:usage: [start_CL num] [end_CL num] [ticket num]"
  exit 1
fi

# p4 get latest
echo "==========Get latest=========="
p4 sync /home/jtsai/Perforce/ksp-automation/depot/release/sz_5.2_alto/scg/...#head
# list all the changed files between start_cl and end_cl
echo "==========Get all changes=========="
p4 files //depot/release/sz_5.2_alto/scg...@$start_cl,$end_cl

# print CL details
# p4 describe -s 796263

# extract cl dates range
# then use the range to print all CLs between the two given cl
# p4 changes -s submitted //depot/release/sz_5.2_alto/scg/control_plane/control/libs...@2020/07/06:19:56:36,2020/07/06:19:59:47
# p4 changes -s submitted //depot/release/sz_5.2_alto/scg/control_plane/control/libs...@2020/07/06:19:56:36,2020/07/06:19:59:47 | wc -lcan tell us how many CLs between start_cl and end_cl


# list all changed files
# p4 files //depot/release/sz_5.2_alto/...@=${start_cl}

